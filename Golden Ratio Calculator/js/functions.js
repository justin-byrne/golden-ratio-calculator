/* Side-Navigation */
/* Show: Child Resolution Selections */
$(document).ready(function() {
    $('.common-res').hide();
    $('.res-button').mouseover(function() { $(this).find('.common-res').show('slow'); });
    $('.res-button').mouseleave(function() { $(this).find('.common-res').hide('slow'); });
});

/* Push: resolutions into Form & Submit */
function setRes(width, height) {
    document.getElementById('a-number').value=width;
    document.getElementById('b-number').value=height;
    document.forms['calc-form'].submit();
}

/* Validate: Form */
function validateForm() {
    var aNum = document.getElementById('a-number').value;
    if (aNum==null || aNum=='') {
        $(function() { $('#empty-value-bubble').hide().fadeIn(500); });
        setTimeout(clearSpeachBubble, 3000);
        return false;
    }
}

function clearSpeachBubble() { $('#empty-value-bubble').fadeOut(1000); }

/* Animate: SVG shimmer from bevel */
// Set: index
var $i = 0;

//  Change: #filter-light attribute for passing bevel effect
$(function() {
    var $light = $('#filter-light');
    // Set: timer, reset after complete 360o pass
    timer = setInterval(function() {
        if($i >= 360) { $i = 0; return; }
        $light.attr('azimuth', $i);
        $i++;
    }, 10);
});