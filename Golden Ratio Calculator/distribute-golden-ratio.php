<?php
    // If: First number ($aBase) is not empty
    if (!empty($_GET['a-number']) && empty($_GET['b-number'])) {

        echo '<div class="width">';                                                                 // Width - Start
        echo '<span><h4>Base Width</h4></span>';                                                    // Width - Base
        echo '<div id="a-base">' . $_GET['a-number'] . 'px' . '</div><!-- #a-base -->';             // A-Base Number

        for ($i = 1; $i < count($a_array); $i++) {
            echo '<div class="a-num">' . round($a_array[$i]) . 'px' . '</div><!-- .a-num -->';      // Show: A-Numbers
        }

        echo '</div><!-- width -->';                                                                // Width - End
        echo '<div class="gutter">&nbsp;</div><!-- .gutter -->';                                    // - - Gutter - -
        echo '<div class="height">';                                                                // Height - Start
        echo '<div id="b-base">&nbsp;</div><!-- #b-base -->';                                       // B-Base Number (Blank)

        for ($i = 1; $i < count($a_array); $i++) {
            echo '<div class="b-num">&nbsp;</div><!-- .b-num -->';                                  // B-Numbers (Blank)
        }

        echo '</div><!-- .height -->';                                                              // Height - End

    } elseif (!empty($_GET['a-number']) && !empty($_GET['b-number'])) {

        echo '<div class="width">';                                                                 // Width - Start
        echo '<span><h4>Base Width</h4></span>';                                                    // Width - Base
        echo '<div id="a-base">' . $_GET['a-number'] . 'px' . '</div><!-- #a-base -->';             // A-Base Number

        for ($i = 1; $i < count($a_array); $i++) {
            if ($a_array[$i] <= 1) {                                                                // If: array value is <= 1 then
                echo '<div class="a-num">' . '&nbsp;' . '</div><!-- .a-num -->';                    // Insert: DIV structure with a '&nbsp;' value
            } else {                                                                                // Else:
                echo '<div class="a-num">' . round($a_array[$i]) . 'px' . '</div><!-- .a-num -->';  // Show: A-Numbers
            }
        }

        echo '</div><!-- width -->';                                                                // Width - End
        echo '<div class="gutter">&nbsp;</div><!-- .gutter -->';                                    // - - Gutter - -
        echo '<div class="height">';                                                                // Height - Start
        echo '<span><h4>Base Height</h4></span>';                                                   // Height - Base
        echo '<div id="b-base">' . $_GET['b-number'] . 'px' . '</div><!-- #b-base -->';             // A-Base Number

        for ($i = 1; $i < count($b_array); $i++) {
            if ($b_array[$i] <= 1) {                                                                // If: array value is <= 1 then
                echo '<div class="b-num">' . '&nbsp;' . '</div><!-- .b-num -->';                    // Insert: DIV structure with a '&nbsp;' value
            } else {                                                                                // Else:
                echo '<div class="b-num">' . round($b_array[$i]) . 'px' . '</div><!-- .b-num -->';  // Show: B-Numbers
            }
        }

        echo '</div><!-- .height -->';                                                              // Height - End

    } else {

        echo '<div class="width">';
        echo '<div id="a-base">&nbsp;</div><!-- #a-base -->';
        echo '</div><!-- .width -->';
        echo '<div class="gutter">&nbsp;</div><!-- .gutter -->';
        echo '<div class="height">';
        echo '<div class="b-num">&nbsp;</div><!-- .b-num -->';
        echo '</div><!-- .height -->';
    }
?>