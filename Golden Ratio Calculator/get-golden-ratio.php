<?php
    $a_number = new FindGoldenRatio;
    $b_number = new FindGoldenRatio;
    $a_array = []; $b_array = [];

    // Identify: whether a-number is set
    if (!empty($_GET['a-number'])) {
        $a_number->setBaseNumber($_GET['a-number']);
        $a_array = $a_number->getRatioArray();
    } else {
        unset($a_number);
    }

    // Identify: whether b-number is set
    if (!empty($_GET['b-number'])) {
        $b_number->setBaseNumber($_GET['b-number']);
        $b_array = $b_number->getRatioArray();

        // Get: count for both a & b numbers, to compare
        $a_array_count = $a_number->getArrayCount();
        $b_array_count = $b_number->getArrayCount();

        // Compare: both a & b array values, then push difference
        if ($a_array_count > $b_array_count) {
            $diff = $a_array_count - $b_array_count;
            $b_number->pushIntoArray($diff);
            $b_array = $b_number->getRatioArray();
        } elseif ($a_array_count < $b_array_count) {
            $diff = $b_array_count - $a_array_count;
            $a_number->pushIntoArray($diff);
            $a_array = $a_number->getRatioArray();
        } else {
            // nothing...
        }

    } else {
        unset($b_number);
    }
?>