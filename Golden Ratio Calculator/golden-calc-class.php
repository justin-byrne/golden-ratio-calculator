<?php
    class FindGoldenRatio {

        private $base_number;
        private $ratio_array = [];

        // Return: the construction of this class when an object is created
        public function __construct() {
            // echo 'Class ' . __CLASS__ . ' was instantiated!' . '<br />';
        }

        // Return: the deconstruction of this class once an object is unset
        public function __destruct() {
            // echo 'Class' . __CLASS__ . ' was destroyed' . '<br />';
        }

        // Set: the base number
        public function setBaseNumber($value) {
            $this->base_number = floatval($value);
            $this->setRatioArrayValues();
            $this->truncateDuplicates();
        }

        // Get: the base number
        public function getBaseNumber() {
            return $this->base_number . '<br />';
        }

        // Set: each descending value of the golden ratio within $ratio_array
        private function setRatioArrayValues() {
            $value = $this->base_number;
            $i = 0;

            if (!function_exists('findRatio')) {
                function findRatio($value) {
                    $result = $value / ((1 + sqrt(5)) / 2);
                    return $result;
                }
            }

            while ($value >= 1) {
                $this->ratio_array[$i] = $value;
                $value = findRatio($value);
                $i++;
            }
        }

        // Truncate: the rounded ultimate value if it equals the rounded penultimate value
        private function truncateDuplicates() {
            $count = count($this->ratio_array) - 1;
            $p = $count--;

            $final_value = round($this->ratio_array[$count]);
            $penultimate_value = round($this->ratio_array[$p]);

            if ($final_value == $penultimate_value) {
                array_pop($this->ratio_array);
            }
        }

        // Get: $ratio_array
        public function getRatioArray() {
            return $this->ratio_array;
        }

        // Get: the current count of $ratio_array
        public function getArrayCount() {
            return count($this->ratio_array);
        }

        // Push: extra NULL values at the end of $ratio_array
        public function pushIntoArray($value) {
            while ($value > 0) {
                $this->ratio_array[] = NULL;
                $value--;
            }
        }
    }   // end of class
?>