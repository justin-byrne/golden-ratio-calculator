<h1>Incrementally Redundant Golden Ratio Calculator</h1>

<p>
	In mathematics, the Golden Ratio (AKA: the divine proportion, golden mean,
	or golden section) can be denoted via the Greek letter phi <i>φ</i>, and
	is represented when the larger value divided by the smaller value is equivocal
	to the whole length of both values divided by the larger value. The golden
	ratio can be commonly found in nature, music, and art, and for some artisans
	it is considered an invaluable tool when attempting to obtain both balance,
	beauty, and aesthetically pleasing proportions that can be used in layout,
	design, and composition.
</p>

<p>
	This application was developed in particular for those uses, in digital art,
	web-application layout and design, rapid prototyping, or any other application
	that could benefit from a continuous abstraction of the golden ratio (ratio
	values) from a single number. This application accepts either a single or two
	numbers (expressed as width & height or Number A and/or B), and then
	proceeds to incrementally process each value, to acquire its other part,
	essentially generating a incrementally based and continuous breakdown of each
	value that (once combined) would result in a single golden ratio, followed
	by the next golden ratio, until the end-result exceeds 0.5.
</p>

<p>
	Practically every web-application currently on the Internet today only
	calculates a single value from an initial value, resulting in two values
	that fit within the context of the golden ratio; however, I needed something
	more. I needed a calculator that could continuously obtain each proceeding
	value's, corresponding to the rule of the golden ratio, to be used throughout
	various aspects of my work, therefore, I developed this application. As far
	as I have seen, there has yet to be an application of this type, which was
	the primary motivator behind developing it, including the initial concept
	that it would be a fantastic tool to utilize while drafting my designs,
	generating aesthetically pleasing layouts and compositions, and more.
</p>

<p>
	Feel free to download this project and either utilize it off-line, or play
	around with its original source code. In addition, there is a link to a live
	(on-line) demo of this application below, and I would be more than happy to
	receive any comments, questions, or criticism. Thank you once again for your
	interest in this project.
</p>

<h1>Live Demo</h1>

<p>
	<a href="http://www.byrne-systems.com/golden-ratio-calculator/">http://www.byrne-systems.com/golden-ratio-calculator/</a>
</p>